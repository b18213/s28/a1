/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses


//Add all of your query/commands here in activity.js

*/

// users
db.users.insertOne(
		{
			firstName : "Leon",
			lastName : "Belmont",
			email: "LBelmont@mail.com",
			password: "lb123",
			isAdmin : false
		
		})

db.users.insertOne(
		{
			firstName : "Trevor",
			lastName : "Belmont",
			email: "TBelmont@mail.com",
			password: "tb123",
			isAdmin : false
		
		})

db.users.insertOne(
		{
			firstName : "Juste",
			lastName : "Belmont",
			email: "JBelmont@mail.com",
			password: "jb123",
			isAdmin : false
		
		})

db.users.insertOne(

		{
			firstName : "Richter",
			lastName : "Belmont",
			email: "RBelmont@mail.com",
			password: "rb123",
			isAdmin : false
		
		})

db.users.insertOne(
		{
			firstName : "Julius",
			lastName : "Belmont",
			email: "JBelmont@mail.com",
			password: "Tb123",
			isAdmin : false
		
		})

// courses
db.courses.insertOne(
		{
			Name : "General Science",
			price : 2000,
			isActive : false
		
		})

db.courses.insertOne(
		{
			Name : "Weapon Training",
			price : 2000,
			isActive : false
		
		})

db.courses.insertOne(
		{
			Name : "Spell Training",
			price : 2000,
			isActive : false
		
		})

// Find all regular/non-admin users
db.users.find({isAdmin: false})

// Update first user as admin
db.users.updateOne({}, 
		{
			$set:
			{
				"isAdmin" : true
			}
		})

// Update one of the courses as active
db.courses.updateOne(
		{
			Name: "Weapon Training"
		}, 
		{
			$set:
			{
				"isActive" : true
			}
		})

// Delete all inactive courses
db.courses.deleteMany({isActive: false})



		